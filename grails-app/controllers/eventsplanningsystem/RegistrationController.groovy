package eventsplanningsystem

import grails.transaction.Transactional

class RegistrationController {

    static allowedMethods = [index: "GET", save: "POST"]

    def index() {
    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view: 'index'
            return
        }

        userInstance.save flush: true
        UserRole.create(userInstance, Role.findByAuthority("ROLE_USER"), true)

        redirect([controller: 'login', action: 'auth'])
    }
}
