package eventsplanningsystem

import grails.plugin.springsecurity.SpringSecurityService
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

class EventController {
    SpringSecurityService springSecurityService
    EventService eventService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def index(Integer max) {
        User user = springSecurityService.currentUser.asType(User.class)
        List<Event> todayEventsList = eventService.getEventsForDay(new Date().clearTime(), user)
        List<Event> tomorrowEventsList = eventService.getEventsForDay(new Date().clearTime().next(), user)
        if(todayEventsList?.size() == 0 && tomorrowEventsList?.size() == 0) {
            flash.error = "You don't have any events for today or tommorow. Create new event or look for events planned for another days."
        }
        respond Event.list(params), model: [todayEventsList: todayEventsList,
                tomorrowEventsList: tomorrowEventsList]
    }


    def browseday() {
        User user = springSecurityService.currentUser.asType(User.class)
        respond Event.list(params), model: [eventsList: eventService.getEventsForDay(params.date, user)]
    }


    def show(Event eventInstance) {
        flash.message = null
        respond eventInstance
    }

    def create() {
        User user = springSecurityService.currentUser.asType(User.class)
        respond new Event(), model: [user: user.getId()]
    }

    @Transactional
    def save(Event eventInstance) {
        if (eventInstance == null) {
            notFound()
            return
        }

        if (eventInstance.hasErrors()) {
            respond eventInstance.errors, view: 'create'
            return
        }
        eventInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'eventInstance.label', default: 'Event'), eventInstance.id])
                redirect eventInstance
            }
            '*' { respond eventInstance, [status: CREATED] }
        }
    }

    def edit(Event eventInstance) {
        User user = springSecurityService.currentUser.asType(User.class)
        respond eventInstance, model: [user: user.getId()]
    }

    @Transactional
    def update(Event eventInstance) {
        if (eventInstance == null) {
            notFound()
            return
        }

        if (eventInstance.hasErrors()) {
            respond eventInstance.errors, view: 'edit'
            return
        }

        eventInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Event.label', default: 'Event'), eventInstance.id])
                redirect eventInstance
            }
            '*' { respond eventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Event eventInstance) {

        if (eventInstance == null) {
            notFound()
            return
        }

        eventInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Event.label', default: 'Event'), eventInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'eventInstance.label', default: 'Event'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
