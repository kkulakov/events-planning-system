package eventsplanningsystem

class Event {

    enum Repeat{
        ONCE('Once'), DAILY('Daily'), WEEKLLY('Weekly'), MONTHLY('Monthly'), YEARLY('Yearly')

        final String value
        Repeat(String value) {
            this.value = value
        }

        String toString() {
            value
        }
    }

    String title
    String description
    Date date
    Repeat repeat


    static belongsTo = [user : User]

    static constraints = {
        title (blank: false)
        description (blank: true)
        date (blank: false)
        repeat (blank: false)
        repeat enumType: 'ordinal'
    }
}
