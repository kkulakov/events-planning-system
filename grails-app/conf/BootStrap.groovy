import eventsplanningsystem.*

class BootStrap {

    def init = { servletContext ->
        def adminRole = Role.findByAuthority("ROLE_ADMIN")
        if(!adminRole)
            adminRole = new Role(authority: "ROLE_ADMIN").save(flush: true)

        def userRole = Role.findByAuthority("ROLE_USER")
        if(!userRole)
            userRole = new Role(authority: "ROLE_USER").save(flush: true)

        User user = User.findByUsername("user")
        if(!user) {
            user = new User(username: "user", password: "pass").save(flush: true)
        }

        UserRole.create(user, userRole, true)
    }
    def destroy = {
    }
}
