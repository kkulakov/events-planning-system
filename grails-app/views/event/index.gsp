
<%@ page import="eventsplanningsystem.Event" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-event" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>

		<div id="list-event" class="content scaffold-list" role="main">
			<g:if test="${flash.error}">
				<div class="message" role="status">${flash.error}</div>
			</g:if>

            <g:if test="${todayEventsList?.size() > 0}">
                <h1>Events for today:</h1></div>
                <table>
                <thead>
                        <tr>

                            <g:sortableColumn property="title" title="${message(code: 'event.title.label', default: 'Title')}" />

                            <g:sortableColumn property="description" title="${message(code: 'event.description.label', default: 'Description')}" />

                            <g:sortableColumn property="date" title="${message(code: 'event.date.label', default: 'Time')}" />

                            <g:sortableColumn property="repeat" title="${message(code: 'event.repeat.label', default: 'Repeat')}" />

                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${todayEventsList}" status="i" var="eventInstance">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                            <td><g:link action="show" id="${eventInstance.id}">${fieldValue(bean: eventInstance, field: "title")}</g:link></td>

                            <td>${fieldValue(bean: eventInstance, field: "description")}</td>

                            <td><g:formatDate format="HH:mm" date="${eventInstance.date}" /></td>

                            <td>${fieldValue(bean: eventInstance, field: "repeat")}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </g:if>
            <g:if test="${tomorrowEventsList?.size()>0}">
                <h1 style="padding-left: 20px;">Events for tomorrow:</h1>
                <table>
                    <thead>
                    <tr>

                        <g:sortableColumn property="title" title="${message(code: 'event.title.label', default: 'Title')}" />

                        <g:sortableColumn property="description" title="${message(code: 'event.description.label', default: 'Description')}" />

                        <g:sortableColumn property="date" title="${message(code: 'event.date.label', default: 'Time')}" />

                        <g:sortableColumn property="repeat" title="${message(code: 'event.repeat.label', default: 'Repeat')}" />

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tomorrowEventsList}" status="i" var="eventInstance">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                            <td><g:link action="show" id="${eventInstance.id}">${fieldValue(bean: eventInstance, field: "title")}</g:link></td>

                            <td>${fieldValue(bean: eventInstance, field: "description")}</td>

                            <td><g:formatDate format="HH:mm" date="${eventInstance.date}" /></td>

                            <td>${fieldValue(bean: eventInstance, field: "repeat")}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <br>
                <br>
            </g:if>
            <h1 style="padding-left: 20px;">Choose date to browse events:</h1>
            <g:form url="[resource:event, action:'browseday']" method="GET" >
                <div class="fieldcontain required">
                    <label for="date">
                        <g:message default="Choose date to show events:" />
                    </label>
                    <g:datePicker name="date" precision="day" />
                    <input type="submit" value="Show" />
                </div>
            </g:form>
		</div>
	</body>
</html>
