<%@ page import="eventsplanningsystem.Event" %>



<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="event.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${eventInstance?.title}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="event.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${eventInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="event.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="minute"  value="${eventInstance?.date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'repeat', 'error')} required">
	<label for="repeat">
		<g:message code="event.repeat.label" default="Repeat" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="repeat" from="${eventsplanningsystem.Event$Repeat?.values()}" keys="${eventsplanningsystem.Event$Repeat.values()*.name()}" required="" value="${eventInstance?.repeat?.name()}" />

</div>

<g:hiddenField name="user" value="${user}" />

