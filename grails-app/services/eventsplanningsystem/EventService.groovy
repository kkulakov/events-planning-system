package eventsplanningsystem

import grails.transaction.Transactional

@Transactional
class EventService {

    List<Event> getEventsForDay(Date day, User user) {
        List<Event> allEvents = Event.findAllByUser(user)
        List<Event> result = allEvents.findAll{ it.date.clone().clearTime() == day }
        for (Event event : allEvents.findAll { it.date.clone().clearTime() < day }) {
            if (event.repeat == Event.Repeat.DAILY) result += event
            else if (event.repeat == Event.Repeat.WEEKLLY && event.date.format("EEE").equals(day.format("EEE"))) result += event
            else if (event.repeat == Event.Repeat.MONTHLY && event.date.format("dd").equals(day.format("dd"))) result += event
            else if (event.repeat == Event.Repeat.YEARLY && event.date.format("dd.MM").equals(day.format("dd.MM"))) result += event
        }

        return result.sort { it.date }
    }
}
